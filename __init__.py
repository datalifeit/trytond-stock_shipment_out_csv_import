# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import stock


def register():
    Pool.register(
        stock.ShipmentOutReturn,
        stock.Configuration,
        stock.CsvImportStart,
        module='stock_shipment_out_csv_import', type_='model')
    Pool.register(
        stock.CsvImport,
        module='stock_shipment_out_csv_import', type_='wizard')
