# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import os
import csv
import tempfile
from datetime import datetime
from collections import OrderedDict
from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelView
from trytond.model.fields import Many2One
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateView, StateAction,
    StateTransition, Button)
from io import StringIO
import ssl
from ftplib import FTP, FTP_TLS
from trytond.config import config
from os import path
from urllib.parse import urlparse

__all__ = ['ShipmentOutReturn', 'Configuration', 'CsvImport', 'CsvImportStart']


class ShipmentOutReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    @classmethod
    def __setup__(cls):
        super(ShipmentOutReturn, cls).__setup__()
        cls._error_messages.update({
            'date_format_error': 'The date format must be "YYYY-MM-DD".',
            'product_not_found': 'Product "%s" not found.',
            'import_error': 'Error while importing file "%s": "%s"'
        })

    @classmethod
    def _get_csv_field_extra_domain(cls, field_name, data):
        domain = []
        if field_name == 'delivery_address':
            domain.append(('party', '=', data['customer'].id))
        return domain

    @classmethod
    def _get_csv_field_value(cls, Model, field_name, field_value, data=None):
        pool = Pool()
        fields = []
        if '.' in field_name:
            fields = field_name.split('.')
            field = fields[0]
        else:
            field = field_name
        if isinstance(Model._fields[field], Many2One):
            field = Model._fields[field]
            relation = field.get_target().__name__
            Model = pool.get(relation)
            if fields:
                domain = cls._get_csv_field_extra_domain(field.name, data)
                domain.append((".".join(fields[1:]), '=', field_value))
                value = Model.search(domain)
                return value[0] if value else None
            else:
                results = Model.search([('id', '=', field_value)])
                return results[0] if results else None
        else:
            if field_name == 'effective_date':
                try:
                    return datetime.strptime(field_value, "%Y-%m-%d").date()
                except ValueError:
                    cls.raise_user_error("date_format_error")
            return field_value

    @classmethod
    def _get_csv_key(cls):
        return ('customer', 'effective_date', 'reference')

    @classmethod
    def import_csv(cls, csv_file, csv_delimiter=','):
        pool = Pool()
        Config = pool.get('stock.configuration')
        config = Config(1)
        Shipment = pool.get('stock.shipment.out.return')
        Company = pool.get('company.company')
        Move = pool.get('stock.move')
        old_shipment = None
        to_del = []
        shipments = []

        config_header = (config.out_return_csv_headers.strip(" "
            ).split(",") if config.out_return_csv_headers else [])

        # On python3 we must convert the binary file to string
        if hasattr(csv_file, 'decode'):
            csv_file = csv_file.decode(errors='ignore')
        csv_file = StringIO(csv_file)
        csv_file = csv.reader(csv_file, delimiter=csv_delimiter)
        headers = next(csv_file)
        unique_key = dict([
            (d, i) for i, d in enumerate(config_header)
            if d in cls._get_csv_key()])

        data = OrderedDict()
        assert len(headers) == len(config_header)
        for row in csv_file:
            _key = tuple(row[value] for value in sorted(
                unique_key.values()))
            if not data.get(_key):
                data.setdefault(_key, {'incoming_moves': []})
            data[_key]['incoming_moves'].append({})
            for i, column in enumerate(config_header):
                if column.startswith('incoming_moves'):
                    _, column = column.split('.', 1)
                    data[_key]['incoming_moves'][-1][column] = row[i]
                else:
                    if not row[i]:
                        continue
                    column_no_dot = column.split(
                        '.')[0] if '.' in column else column
                    row[i] = cls._get_csv_field_value(cls, column, row[i],
                        data=data[_key])
                    data[_key][column_no_dot] = row[i]

            company = Company(Transaction().context['company'])
        for k, v in data.items():
            shipment = None
            if k[0]:
                old_shipment = None
                domain = [(k, '=', v.get(k)) for k in cls._get_csv_key()]
                shipment = Shipment.search(domain, limit=1)
                if shipment:
                    shipment = cls._check_csv_shipment(shipment)
                    if not shipment:
                        continue
                    if v['incoming_moves'][0]:
                        to_del.extend(list(shipment.incoming_moves))
            else:
                shipment = old_shipment
            if not shipment:
                shipment = Shipment()
            moves = []
            _move_data = v.pop('incoming_moves')
            if not old_shipment:
                for k2, v2 in v.items():
                    setattr(shipment, k2, v2)
            for move_values in _move_data:
                if not move_values:
                    continue

                move = Move(
                    company=company,
                    currency=company.currency,
                    effective_date=shipment.effective_date)
                for move_key, move_value in move_values.items():
                    _value = cls._get_csv_field_value(
                        Move, move_key, move_value)
                    setattr(move, move_key.split('.')[0], _value)
                move.from_location = (shipment.
                    on_change_with_customer_location())
                move.to_location = (shipment.
                    on_change_with_warehouse_input())
                move.quantity = float(move.quantity)
                if not move.product:
                    cls.raise_user_error('product_not_found',
                        move_values.get('product', ''))
                move.on_change_product()
                moves.append(move)

            if not old_shipment:
                shipment.incoming_moves = moves
                shipments.append(shipment)
                old_shipment = shipment
            else:
                shipment.incoming_moves = list(
                    shipment.incoming_moves) + moves

        if shipments:
            Shipment.save(shipments)
        if to_del:
            Move.delete(to_del)

        return shipments

    @classmethod
    def _check_csv_shipment(cls, shipments):
        value = shipments[0]
        if value.state == 'draft':
            return value

    @classmethod
    def cron_import_csv_from_ftp(cls, folder_path='', csv_delimiter=','):
        url = urlparse(config.get('ftp_shipment', 'url'))
        backup_folder = config.get('ftp_shipment', 'backup')
        tls = config.getboolean('ftp_shipment', 'tls', default=False)
        tls_secure = config.getboolean('ftp_shipment', 'tls_secure',
            default=True)

        if tls:
            ftp = FTP_TLS()
            ftp.ssl_version = ssl.PROTOCOL_SSLv23
            ftp.connect(url.hostname)
            ftp.login(url.username, url.password, secure=tls_secure)
        else:
            ftp = FTP(url.hostname)
            ftp.login(url.username, url.password)
        ftp.cwd(folder_path)
        imported_files = []
        try:
            filename = ''
            for filename in ftp.nlst('*.csv'):
                fileno, fname = tempfile.mkstemp('.csv', 'tryton_')
                handle = open(fname, 'wb')
                res = ftp.retrbinary('RETR %s' % filename,
                    callback=handle.write)
                handle.close()
                cls.import_csv(open(fname, 'rb').read(),
                    csv_delimiter=csv_delimiter)
                imported_files.append(filename)
        except Exception as e:
            ftp.quit()
            with Transaction().new_transaction():
                cls.raise_user_error('import_error', (filename, e.message))

        if backup_folder not in ftp.nlst():
            ftp.mkd(backup_folder)
        for filename in imported_files:
            ftp.rename(filename, path.join(backup_folder, filename))

        ftp.quit()

    @classmethod
    def cron_import_csv_from_folder(cls, folder_path, csv_delimiter=','):
        imported_files = []
        try:
            for filename in os.listdir(folder_path):
                if not filename.endswith('.csv'):
                    continue
                fname = os.path.join(folder_path, filename)
                cls.import_csv(open(fname, 'rb').read(),
                    csv_delimiter=csv_delimiter)
                imported_files.append(filename)
        except Exception as e:
            with Transaction().new_transaction():
                cls.raise_user_error('import_error', (filename, e.message))

        if not imported_files:
            return

        if not os.path.isdir(os.path.join(folder_path, 'backup')):
            os.mkdir(path)
        for filename in imported_files:
            os.rename(os.path.join(folder_path, filename),
                os.path.join(folder_path, 'backup', filename))


class CsvImport(Wizard):
    """Shipment out CSV Import"""
    __name__ = 'stock.shipment.out.csv_import'

    start = StateView(
        'stock.shipment.out.csv_import.start',
        'stock_shipment_out_csv_import.csv_import_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Import', 'pre_import', 'tryton-ok', default=True),
        ])
    pre_import = StateTransition()
    import_out_return = StateAction('stock.act_shipment_out_return_form')

    def transition_pre_import(self):
        return 'import_%s' % self.start.type

    def _do_import(self, action):
        pool = Pool()
        Shipment = pool.get('stock.shipment.%s' % self.start.type.replace(
            '_', '.'))
        csv_file = self.start.csv_file
        shipments = Shipment.import_csv(csv_file,
            csv_delimiter=str(self.start.delimiter))
        return {'res_id': [i.id for i in shipments]}

    def do_import_out_return(self, action):
        data = self._do_import(action)
        return action, data


class CsvImportStart(ModelView):
    """Import CSV View"""
    __name__ = 'stock.shipment.out.csv_import.start'

    csv_file = fields.Binary('File', required=True)
    type = fields.Selection([
        ('out_return', 'Out Return')
        ], 'Type', required=True)
    delimiter = fields.Selection([
        (',', ','),
        (';', ';')], 'Delimiter', required=True)

    @staticmethod
    def default_type():
        return 'out_return'

    @staticmethod
    def default_delimiter():
        return ','


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    out_return_csv_headers = fields.Char('Shipment out Return CSV Headers')
